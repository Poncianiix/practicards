module.exports = (sequelize, type) => {
    const Departamento = sequelize.define('departamentos', {
      id_departamento: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrement: true

      },
      nombre_departamento: type.STRING,
      region: type.STRING
    });
  
    return Departamento;
  };
  