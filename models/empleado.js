module.exports = (sequelize, type) => {
    const Empleado = sequelize.define('empleados', {
      id_empleado: {
        type: type.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      nombre_empleado: type.STRING,
      apellido_empleado: type.STRING,
    
    });
  
    return Empleado;
  };
  