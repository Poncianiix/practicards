# Practica con RDS


## Para hacerlo funcionar


1. Clonamos el repositorio.
2. Dentro de la carpeta en donde se clonó abrimos una terminal.
3. Ejecutamos el siguiente comando:
```javascript
npm install
```
5. Abrimos el archivo db.js y desde la línea 6 hasta la 9 tendremos este código:
```javascript
const sequelize = new Sequelize('db1', 'admin', 'abcd1234', {
 host: 'database-1.cabqac5smnov.us-east-1.rds.amazonaws.com',
 dialect: 'mysql'
});
```
6. Cambiamos la configuración conforme a nuestra base de datos.
7. Guardamos los cambios.
8. Ejecutamos el comando:
```javascript
npm run dev
```
9. Si todo salio bien nos saldrá el mensaje "Conexión exitosa a la base de datos".




## Código por si queremos crear la tabla de departamentos manualmente


```sql
CREATE TABLE departamentos (
 id_departamento INT PRIMARY KEY,
 nombre_departamento VARCHAR(50),
 region VARCHAR(50),
 createdAt DATETIME NOT NULL,
 updatedAt DATETIME NOT NULL
 );
```
## Código por si queremos crear la tabla de empleados


```sql
CREATE TABLE empleados (
 id_empleado INT PRIMARY KEY,
 nombre_empleado VARCHAR(50),
 apellido_empleado VARCHAR(50),
 id_departamento INT,
  createdAt DATETIME NOT NULL,
 updatedAt DATETIME NOT NULL,
 FOREIGN KEY (id_departamento) REFERENCES departamentos(id_departamento)
);
```
## Configuración del postman

El URL que tenemos que poner en postman es el siguiente: 'localhost:3000/empleados/'
Si es para departamentos ponemos este otro: 'localhost:3000/departamentos/'

## Json para crear empleados y departamentos

```json
{
  "nombre_empleado": "Juan",
  "apellido_empleado": "Pérez",
  "id_departamento": 1
}
```
```json
{
    "nombre_departamento": "Departamento de Ventas",
    "region":"América"
}
```