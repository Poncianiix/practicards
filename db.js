const Sequelize = require('sequelize');

const departamentoModel = require('./models/departamento');
const empleadoModel = require('./models/empleado');

const sequelize = new Sequelize('db1', 'admin', 'abcd1234', {
  host: 'database-1.cabqac5smnov.us-east-1.rds.amazonaws.com',
  dialect: 'mysql'
});



const Departamento = departamentoModel(sequelize, Sequelize);
const Empleado = empleadoModel(sequelize, Sequelize);

// Establece relaciones entre las tablas
Departamento.hasMany(Empleado, {as: 'empleados', foreignKey: 'id_departamento'});
Empleado.belongsTo(Departamento, {as: 'departamento', foreignKey: 'id_departamento'});


sequelize.sync({
    force: false
}).then(() => {
  console.log("Conexión exitosa a la base de datos");
});

module.exports = { Departamento, Empleado };
