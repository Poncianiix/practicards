const {Departamento} = require('../db');

function list(req, res, next) {
    Departamento.findAll({include:['empleados']})
         .then(objects => res.json(objects))
         .catch(error => res.send(error));
}

function index(req, res, next) {
    const id = req.params.id;
    Departamento.findByPk(id)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}

function create(req, res, next) {
    const nombre_departamento = req.body.nombre_departamento;
    const region = req.body.region;

    let departamento = new Object({
        nombre_departamento: nombre_departamento,
        region: region
    });
    
    Departamento.create(departamento)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}

function replace(req, res, next) {
    const id = req.params.id;
    Departamento.findByPk(id).then(obj => {
        const nombre_departamento = req.body.nombre_departamento ? req.body.nombre_departamento:""
        const region = req.body.region ? req.body.region:"";
        obj.update({
            nombre_departamento: nombre_departamento,
            region: region
        }).then(Departamento => res.json(Departamento))
          .catch(err => res.send(err));
    }).catch(err => res.send(err));
}

function update(req, res, next) {
    const id = req.params.id;
    Departamento.findByPk(id).then(obj => {
        const nombre_departamento = req.body.nombre_departamento ? req.body.nombre_departamento:obj.nombre_departamento
        const region = req.body.region ? req.body.region:obj.region;
        obj.update({
            nombre_departamento: nombre_departamento,
            region: region
        }).then(Departamento => res.json(Departamento))
          .catch(err => res.send(err));
    }).catch(err => res.send(err));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Departamento.destroy({where:{id_departamento:id}})
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}

module.exports = {list,index,create,replace,update,destroy};
