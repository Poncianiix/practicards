const express = require('express');
const {Empleado} = require('../db');

function list(req, res, next) {
    Empleado.findAll({
      include: ['departamento']
    })
      .then(objects => res.json(objects))
      .catch(error => res.send(error));
  }
  

function index(req, res, next) {
    const id = req.params.id;
    Empleado.findByPk(id)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}

function create(req, res, next) {
    const nombre_empleado = req.body.nombre_empleado;
    const apellido_empleado = req.body.apellido_empleado;
    const id_departamento = req.body.id_departamento;

    let empleado = new Object({
        nombre_empleado:nombre_empleado,
        apellido_empleado:apellido_empleado,
        id_departamento:id_departamento
    });
    
    
    Empleado.create(empleado)
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}

function replace(req, res, next) {
    const id = req.params.id;
    Empleado.findByPk(id).then(obj => {
        const nombre_empleado = req.body.nombre_empleado ? req.body.nombre_empleado:""
        const apellido_empleado = req.body.apellido_empleado ? req.body.apellido_empleado:"";
        const id_departamento = req.body.id_departamento ? req.body.id_departamento:"";
        obj.update({
            nombre_empleado:nombre_empleado,
            apellido_empleado:apellido_empleado,
            id_departamento:id_departamento
        }).then(Empleado => res.json(Empleado))
          .catch(err => res.send(err));
    }).catch(err => res.send(err));
}

function update(req, res, next) {
    const id = req.params.id;
    Empleado.findByPk(id).then(obj => {
        const nombre_empleado = req.body.nombre_empleado ? req.body.nombre_empleado:obj.nombre_empleado
        const apellido_empleado = req.body.apellido_empleado ? req.body.apellido_empleado:obj.apellido_empleado;
        const id_departamento = req.body.id_departamento ? req.body.id_departamento:obj.id_departamento;
        obj.update({
            nombre_empleado:nombre_empleado,
            apellido_empleado:apellido_empleado,
            id_departamento:id_departamento
        }).then(Empleado => res.json(Empleado))
          .catch(err => res.send(err));
    }).catch(err => res.send(err));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Empleado.destroy({where:{id_empleado:id}})
         .then(obj => res.json(obj))
         .catch(err => res.send(err));
}

module.exports = {list,index,create,replace,update,destroy};